# Requirements
# Doxygen
sudo dnf --enablerepo=crb install doxygen -y

python3 -m pip install pytest pytest-xdist pyyaml

git clone https://github.com/open-quantum-safe/liboqs.git
git clone https://github.com/open-quantum-safe/oqs-provider.git