PATH="$PATH:/vagrant/bin/"

cd liboqs
sudo mkdir -p /opt/liboqs
cmake -GNinja -DBUILD_SHARED_LIBS=ON -DCMAKE_INSTALL_PREFIX=/opt/liboqs -S .
ninja
ninja install
cd ..
