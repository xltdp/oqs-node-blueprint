export PATH="$PATH:/vagrant/bin/"
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/opt/liboqs:/opt/liboqs/lib64:/opt/liboqs/include"

cd oqs-provider
sudo mkdir -p /opt/oqs-provider
cmake -GNinja -DCMAKE_INSTALL_PREFIX=/opt/oqs-provider -S .
ninja 
cd ..
